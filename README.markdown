# consul-enterprise-network-segments

A self-container Docker Compose environemnt for experimenting with Consul Enterprise Network Segments.

## Requirements

* [Docker Compose](https://docs.docker.com/compose/)
* [Consul Enterprise](https://www.consul.io/docs/enterprise/network-segments/index.html) binary in ./docker/binaries. If you have the required AWS S3 credentials as envvars you can use the ./scripts/download-consul-enterprise-bins.sh.

### optionally
* [httpie](https://httpie.org)
* [Python 'em' package](https://pypi.python.org/pypi/em/)
* [jq](https://stedolan.github.io/jq/)

## Usage


### start the environment
```pre
# export AWS_ACCESS_KEY_ID=**********
# export AWS_SECRET_ACCES_KEY=**********
$ ./scripts/download-consul-enterprise-bin.sh
$ docker-compose up -d --build

$ export CONSUL_HTTP_ADDR=http://localhost:8500
$ consul members
Node     Address          Status  Type    Build      Protocol  DC   Segment
consul0  172.24.0.2:8301  alive   server  1.0.0+ent  2         dc1  <all>
consul1  172.24.0.3:8301  alive   server  1.0.0+ent  2         dc1  <all>
consul2  172.24.0.4:8301  alive   server  1.0.0+ent  2         dc1  <all>

$ consul operator raft list-peers
Node     ID                                    Address          State     Voter  RaftProtocol
consul0  cc415722-e7b3-bdad-15ff-419cd74e6ab1  172.24.0.2:8300  leader    true   3
consul2  7f37159e-faa9-82a2-b3b1-466391adc3d1  172.24.0.4:8300  follower  true   3
consul1  dc0bfa4b-b958-4dcf-7407-dfa16b8502cc  172.24.0.3:8300  follower  true   3
```

### see the list of configured network segments

Using [httpie](https://httpie.org/) and [em](https://pypi.python.org/pypi/em/) to see the list of segments (denoted by 'sl_' prefix):

```pre
$ http http://localhost:8500/v1/agent/members | jq . | em sl_                             
[                                                                   
  {                                                                 
    "Name": "consul2",                                              
    "Addr": "172.24.0.4",                                           
    "Port": 8301,                                                   
    "Tags": {                                                       
      "build": "1.0.0+ent:4dee959",                                 
      "dc": "dc1",                                                  
      "id": "7f37159e-faa9-82a2-b3b1-466391adc3d1",                 
      "port": "8300",                                               
      "raft_vsn": "3",                                              
      "role": "consul",                                             
      "segment": "",                                                
      "sl_alpha": "172.24.0.4:8900",                                
      "sl_beta": "172.24.0.4:8901",                                 
      "sl_gamma": "172.24.0.4:8902",                                
      "vsn": "2",                                                   
      "vsn_max": "3",                                               
      "vsn_min": "2",                                               
      "wan_join_port": "8302"                                       
    },                                                              
    "Status": 1,                                                    
    "ProtocolMin": 1,                                               
    "ProtocolMax": 5,                                               
    "ProtocolCur": 2,                                               
    "DelegateMin": 2,                                               
    "DelegateMax": 5,                                               
    "DelegateCur": 4                                                
  },                                                                
  {                                                                 
    "Name": "consul0",                                              
    "Addr": "172.24.0.2",                                           
    "Port": 8301,                                                   
    "Tags": {                                                       
      "bootstrap": "1",                                             
      "build": "1.0.0+ent:4dee959",                                 
      "dc": "dc1",                                                  
      "id": "cc415722-e7b3-bdad-15ff-419cd74e6ab1",                 
      "port": "8300",                                               
      "raft_vsn": "3",                                              
      "role": "consul",                                             
      "segment": "",                                                
      "sl_alpha": "172.24.0.2:8900",                                
      "sl_beta": "172.24.0.2:8901",                                 
      "sl_gamma": "172.24.0.2:8902",                                
      "vsn": "2",                                                   
      "vsn_max": "3",                                               
      "vsn_min": "2",                                               
      "wan_join_port": "8302"                                       
    },                                                              
    "Status": 1,                                                    
    "ProtocolMin": 1,                                               
    "ProtocolMax": 5,                                               
    "ProtocolCur": 2,                                               
    "DelegateMin": 2,                                               
    "DelegateMax": 5,                                               
    "DelegateCur": 4                                                
  },                                                                
  {                                                                 
    "Name": "consul1",                                              
    "Addr": "172.24.0.3",                                           
    "Port": 8301,                                                   
    "Tags": {                                                       
      "build": "1.0.0+ent:4dee959",                                 
      "dc": "dc1",                                                  
      "id": "dc0bfa4b-b958-4dcf-7407-dfa16b8502cc",                 
      "port": "8300",                                               
      "raft_vsn": "3",                                              
      "role": "consul",                                             
      "segment": "",                                                
      "sl_alpha": "172.24.0.3:8900",                                
      "sl_beta": "172.24.0.3:8901",                                 
      "sl_gamma": "172.24.0.3:8902",                                
      "vsn": "2",                                                   
      "vsn_max": "3",                                               
      "vsn_min": "2",                                               
      "wan_join_port": "8302"                                       
    },                                                              
    "Status": 1,                                                    
    "ProtocolMin": 1,                                               
    "ProtocolMax": 5,                                               
    "ProtocolCur": 2,                                               
    "DelegateMin": 2,                                               
    "DelegateMax": 5,                                               
    "DelegateCur": 4                                                
  }                                                                 
]
```

## Configs

* Most of the configs are baked into the Docker image via a COPY in the ```Dockerfile``` (see ./docker/configs) and are located in /etc/hashicorp/consul.
* A handful of config options used for bootstrapping the cluster are specified in ```docker-compose.yml```.

* To rebuild the images if you've made your own changes: ```docker-compose down -v && docker-compose up --build -d)```

## Contact

* Nathan Valentine - nrvale00 gmail com | nathan hashicorp com
