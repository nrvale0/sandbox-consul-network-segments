server = true
ui = true
data_dir = "/var/hashicorp/consul"
disable_host_node_id = true
client_addr = "0.0.0.0"
raft_protocol = 3

